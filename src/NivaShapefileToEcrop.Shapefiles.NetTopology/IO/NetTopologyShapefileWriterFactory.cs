/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System.Text;
using NivaShapefileToEcrop.Shapefiles.Core;
using NivaShapefileToEcrop.Shapefiles.Core.IO;

namespace NivaShapefileToEcrop.Shapefiles.NetTopology.IO
{
	public class NetTopologyShapefileWriterFactory : IShapefileWriterFactory
	{
		private readonly int _srid;
		private readonly Encoding _encoding;

		public NetTopologyShapefileWriterFactory() : this(SpatialReferenceIds.LatLon)
		{

		}

		public NetTopologyShapefileWriterFactory(int srid) : this(srid, Encoding.UTF8)
		{

		}

		public NetTopologyShapefileWriterFactory(int srid, Encoding encoding)
		{
			_srid = srid;
			_encoding = encoding;
		}

		public IShapefileWriter Create()
		{
			return new NetTopologyShapefileWriter(_srid, _encoding);
		}
	}
}