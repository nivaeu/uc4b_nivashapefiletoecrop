/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System;
using System.Collections.Generic;
using System.IO;

namespace NivaShapefileToEcrop.Shapefiles.Core.IO.Streams
{
	/// <summary>
	/// Uses a directory as source and creates providers from existing or new files.
	/// </summary>
	public class FileStreamProviderRegistry : IStreamProviderRegistry
	{
		private readonly DirectoryInfo _directory;
		private readonly bool _isReadOnly;
		private IDictionary<string, string> _maps;

		/// <summary>
		/// Prefix for keys used in lookup.
		/// </summary>
		public string Prefix { get; set; }

		public bool IsReadOnly => _isReadOnly;

		public FileStreamProviderRegistry(string path, bool isReadOnly) : this(new DirectoryInfo(path), isReadOnly)
		{

		}

		public FileStreamProviderRegistry(DirectoryInfo directory, bool isReadOnly)
		{
			_directory = directory ?? throw new ArgumentException("Directory is null.", nameof(directory));
			if (!directory.Exists)
				throw new ArgumentException("Directory does not exist.", nameof(directory));
			
			_isReadOnly = isReadOnly;
		}

		public void Map(string key, string value)
		{
			if (_maps == null)
				_maps = new Dictionary<string, string>();
			_maps[key] = value;
		}

		public string Map(string key)
		{
			if (_maps != null && _maps.TryGetValue(key, out var mapped))
					return mapped;
			return key;
		}

		public IStreamProvider GetProvider(string key)
		{
			var mapped = Map(key);
			if (!string.IsNullOrEmpty(Prefix))
				mapped = Prefix + mapped;

			var path = Path.Combine(_directory.FullName, mapped);
			var file = new FileInfo(path);
			return new FileStreamProvider(file, _isReadOnly);
		}
	}
}
