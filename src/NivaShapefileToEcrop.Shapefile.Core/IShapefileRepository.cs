﻿using System.IO;
using NivaShapefileToEcrop.Shapefiles.Core.Models;

namespace NivaShapefileToEcrop.Shapefiles.Core
{
    public interface IShapefileRepository
    {
        bool ShapefileExists(int id);
        Shapefile GetShapefile(int id);
        Shapefile CreateShapefile(CreateShapefile model);
        int UpdateFileData(int id, Stream stream);
        Stream GetFileData(int id);
        int DeleteShapefile(int id);
    }
}