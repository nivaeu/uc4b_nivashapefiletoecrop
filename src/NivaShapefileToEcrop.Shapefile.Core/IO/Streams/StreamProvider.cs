/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System;
using System.IO;

namespace NivaShapefileToEcrop.Shapefiles.Core.IO.Streams
{
	public class StreamProvider : IStreamProvider
	{
		public bool AutoReset { get; set; }
		public bool IsReadOnly { get; }

		private readonly Func<Stream> _factory;

		public StreamProvider(Stream stream, bool isReadOnly)
		{
			if(stream == null)
				throw new ArgumentException("Argument can not be null.", nameof(stream));

			_factory = () => stream;
			IsReadOnly = isReadOnly;
		}

		public StreamProvider(Func<Stream> factory, bool isReadonly)
		{
			_factory = factory;
			IsReadOnly = isReadonly;
		}
		
		public Stream OpenRead()
		{
			return GetStream();
		}

		public Stream OpenWrite(bool truncate)
		{
			if (IsReadOnly)
				throw new NotSupportedException("Stream provider is read only.");

			var stream = GetStream();
			if (truncate)
				stream.SetLength(0);

			return stream;
		}

		internal Stream GetStream()
		{
			var stream = _factory();
			if (AutoReset)
				stream.Seek(0, SeekOrigin.Begin);
			return stream;
		}
	}
}
