﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NivaIsoXmlToEcrop.Mapper.Models;

namespace Niva.Common.Models.ECropReportMessage
{
	/// <summary>
	/// A GeoJSON object, specifying the crop plot location in lat-lon decimal degrees, GEoJSON
	/// uses the WGS84 datum.
	/// </summary>
	public partial class PhysicalSpecifiedGeographicalFeature
	{
		/// <summary>
		/// The geometry of this specific feature
		/// </summary>
		[JsonProperty("geometry", Required = Required.Always)]
		[JsonConverter(typeof(GeometryConverter))]
		// ReSharper disable once InconsistentNaming
		public Geometry geometry { get; set; }

		/// <summary>
		/// The geographical type, currently 'Feature'.
		/// </summary>
		[JsonProperty("type", Required = Required.Always)]
		[JsonConverter(typeof(StringEnumConverter))]
		// ReSharper disable once InconsistentNaming
		public PhysicalSpecifiedGeographicalFeatureType type { get; set; }
	}
}