/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System.Collections.Generic;
using NivaShapefileToEcrop.Shapefiles.Core.Models;

namespace NivaShapefileToEcrop.Shapefiles.Core.IO
{
	public interface IShapefileReader
	{
		IEnumerable<ShapefileColumn> ReadColumns();
		IEnumerable<ShapefileGeometry> ReadGeometries();
		IEnumerable<ShapefileFeature> ReadFeatures();
	}
}