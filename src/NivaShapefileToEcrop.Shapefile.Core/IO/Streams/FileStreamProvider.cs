/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System;
using System.IO;

namespace NivaShapefileToEcrop.Shapefiles.Core.IO.Streams
{
	public class FileStreamProvider : IStreamProvider
	{
		public bool IsReadOnly => _isReadOnly && _file.IsReadOnly;

		private readonly bool _isReadOnly;
		private readonly FileInfo _file;

		public FileStreamProvider(string filename, bool isReadOnly)
		{
			_file = new FileInfo(filename);
			_isReadOnly = isReadOnly;
		}

		public FileStreamProvider(FileInfo file, bool isReadOnly)
		{
			_file = file ?? throw new ArgumentException("Argument can not be null.", nameof(file));
			_isReadOnly = isReadOnly;
		}

		public Stream OpenRead()
		{
			return _file.OpenRead();
		}

		public Stream OpenWrite(bool truncate)
		{
			if (IsReadOnly)
				throw new NotSupportedException("Stream provider is read only.");

			var stream = _file.Open(FileMode.OpenOrCreate);
			if (truncate)
				stream.SetLength(0);

			return stream;
		}
	}
}
