/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/


namespace NivaShapefileToEcrop.Shapefiles.Core.Models
{
	public static class ShapefileColumnTypes
	{
		public const char Character = 'C';
		public const char Number = 'N';
		public const char FloatingPoint = 'F';
		public const char Logical = 'L';
		public const char Date = 'D';
	}
}