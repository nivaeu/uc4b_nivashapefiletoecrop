/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

namespace NivaShapefileToEcrop.Shapefiles.Core.Models
{
	public class ShapefileColumn
	{
		public string Name { get; set; }
		public char Type { get; set; }
		public int Length { get; set; }
		public int DecimalCount { get; set; }

		public static ShapefileColumn Number(string name)
		{
			return new ShapefileColumn
			{
				Name = name,
				DecimalCount = 15,
				Length = 8,
				Type = ShapefileColumnTypes.Number
			};
		}

		public static ShapefileColumn FloatingPoint(string name, int decimalCount)
		{
			return new ShapefileColumn
			{
				Name = name,
				DecimalCount = decimalCount,
				Length = 4,
				Type = ShapefileColumnTypes.FloatingPoint
			};
		}

		public static ShapefileColumn Character(string name, int length)
		{
			return new ShapefileColumn
			{
				Name = name,
				Length = length,
				Type = ShapefileColumnTypes.Character
			};
		}
	}
}
