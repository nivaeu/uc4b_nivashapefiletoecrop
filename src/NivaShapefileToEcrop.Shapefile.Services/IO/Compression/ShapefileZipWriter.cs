/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System.IO;
using System.IO.Compression;
using NivaShapefileToEcrop.Shapefiles.Core.IO;

namespace NivaShapefileToEcrop.Shapefiles.Services.IO.Compression
{
	/// <summary>
	/// Use in conjunction with a shapefile writer to write shapefile to a zip archive.
	/// </summary>
	public class ShapefileZipWriter
	{
		private readonly IShapefileWriter _writer;

		public ShapefileZipWriter(IShapefileWriter writer)
		{
			_writer = writer;
		}

		/// <summary>
		/// Write zip archive to a stream.
		/// </summary>
		/// <param name="stream">Stream to write to.</param>
		/// <param name="name">Name of shapefile in archive without extension.</param>
		/// <param name="leaveOpen">Leave stream open after writing.</param>
		public void WriteTo(Stream stream, string name, bool leaveOpen)
		{
			using (var archive = new ZipArchive(stream, ZipArchiveMode.Create, true))
			{
				// TODO: The current (outdated) version of NetTopology only supports these 3 shapefile files, this should be made dynamic to handle future updates.
				archive.CreateEntry(name + ShapefileFileExtensions.Main);
				archive.CreateEntry(name + ShapefileFileExtensions.Dbf);
				archive.CreateEntry(name + ShapefileFileExtensions.Index);
			}

			stream.Seek(0, SeekOrigin.Begin);
			using (var archive = new ZipArchive(stream, ZipArchiveMode.Update, leaveOpen))
			{
				var registry = new ShapefileZipStreamProviderRegistry(archive, false);
				_writer.WriteTo(registry);
			}
		}
		
		/// <summary>
		/// Write zip archive to a file.
		/// </summary>
		/// <param name="filename">Filename of zip archive. Name is used for shapefile as well.</param>
		public void WriteToFile(string filename)
		{
			var path = Path.GetDirectoryName(filename);
			if (!Directory.Exists(path))
				throw new DirectoryNotFoundException($"Directory {path} not found.");

			var name = Path.GetFileNameWithoutExtension(filename);

			var shpFile = new FileInfo(Path.Combine(path, name + ShapefileFileExtensions.Main));
			var dbfFile = new FileInfo(Path.Combine(path, name + ShapefileFileExtensions.Dbf));
			var shxFile = new FileInfo(Path.Combine(path, name + ShapefileFileExtensions.Index));
			var zipFile = new FileInfo(Path.Combine(path, name + ".zip"));
			
			_writer.WriteTo(filename);

			using (var archive = new ZipArchive(zipFile.Open(FileMode.OpenOrCreate), ZipArchiveMode.Create))
			{
				WriteEntry(archive, shpFile);
				WriteEntry(archive, dbfFile);
				WriteEntry(archive, shxFile);
			}
		}

		internal Stream CreateArchive(Stream stream)
		{
			new ZipArchive(stream, ZipArchiveMode.Create, true).Dispose();
			stream.Seek(0, SeekOrigin.Begin);
			return stream;
		}

		internal Stream CreateArchive()
		{
			return CreateArchive(new MemoryStream());
		}

		internal ZipArchiveEntry WriteEntry(ZipArchive archive, FileInfo file)
		{
			using (var stream = file.OpenRead())
				return WriteEntry(archive, stream, file.Name);
		}

		internal ZipArchiveEntry WriteEntry(ZipArchive archive, Stream stream, string filename)
		{
			var entry = archive.CreateEntry(filename);

			using (var entryStream = entry.Open())
				stream.CopyTo(entryStream);

			return entry;
		}
	}
}
