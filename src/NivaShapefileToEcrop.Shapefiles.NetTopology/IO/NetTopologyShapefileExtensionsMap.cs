/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System;
using NivaShapefileToEcrop.Shapefiles.Core.IO;
using NTS = NetTopologySuite.IO.Streams;

namespace NivaShapefileToEcrop.Shapefiles.NetTopology.IO
{
	public static class NetTopologyShapefileExtensionsMap
	{
		public static string ToStreamType(string extension)
		{
			switch (extension)
			{
				case ShapefileFileExtensions.Main: return NTS.StreamTypes.Shape;
				case ShapefileFileExtensions.Dbf: return NTS.StreamTypes.Data;
				case ShapefileFileExtensions.Index: return NTS.StreamTypes.Index;
				default:
					throw new NotSupportedException($"Extension {extension} not supported.");
			}
		}

		public static string ToExtension(string streamType)
		{
			switch (streamType)
			{
				case NTS.StreamTypes.Shape:
					return ShapefileFileExtensions.Main;
				case NTS.StreamTypes.Data:
					return ShapefileFileExtensions.Dbf;
				case NTS.StreamTypes.Index:
					return ShapefileFileExtensions.Index;
				default:
					throw new NotSupportedException($"Stream type not supported {streamType}.");
			}
		}
	}
}