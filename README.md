# Introduction
This subproject is part of the ["New IACS Vision in Action” NIVA](https://www.niva4cap.eu/) project that delivers 
a suite of digital solutions, e-tools and good practices for e-governance and initiates an innovation ecosystem to 
support further development of IACS that will facilitate data and information flows.

This project has received funding from the European Union’s Horizon 2020 research and innovation programme under 
grant agreement No 842009.

Please visit the [website](https://www.niva4cap.eu) for further information. A complete list of the sub-projects 
made available under the NIVA project can be found on [gitlab](https://gitlab.com/nivaeu/)

# NivaShapefileToEcrop
NivaShapefileToEcrop is a library for mapping parts of Shapefile to an EcropReportMessage.
This repository contains a proof of concept developed at SEGES (https://seges.dk/)

## Purpose

## Library Limits
The current implementation of library put the responsibility on the caller to know the name of shapefile attribute that contains the quantity data to extract. The reason for this is that different company has different names e.g., Quantity, AppliedRate, AppliedRat.

The library was intended to be standard 2.0, but because it relies on an older version of NetTopologySuite that only developed to .net framework. We intent to upgrade NetTopologySuite version 2 in the future.


## Getting Started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

## Prerequisites
Necessary software to use this repository

- **Visual Studio 2019:** https://visualstudio.microsoft.com/downloads/
- **Net Core 5.0:** https://dotnet.microsoft.com/download/dotnet/5.0
- **git:** https://git-scm.com/downloads

## Installing

## Example
Example of usage of Shapefile mapper. ColumnName define the shapefile attribute data extract from and Id which shapefile in the folder data is extract from, since there can be more than shapefile in the zipped folder
```C#
var ecropJson = File.ReadAllText("TestData/partialEcropMessage.json");
var partialECropMessage = JsonConvert.DeserializeObject<EcropMessage>(ecropJson);
var zipShapefile = new MemoryStream(File.ReadAllBytes("TestData/Export_20210819_1419.zip"));
var reader = new NetTopologyShapefileReaderFactory();
var input = new PartialECropShapefileInput
{
	ZipShapefile = zipShapefile,
	ECropMessage = partialECropMessage,
	Id = "54_Application_2021-07-29_00",
	UnitInShapefile = "kg/ha",
	ColumnName = "AppliedRate"
};
var mapper = new ECropMapper(reader);
var result = mapper.Map(input);
```

## Running the tests
1. cd `<root directory>`
2. dotnet test


## License
EU-PL Nest framework is <a rel="nofollow noreferrer noopener" href="https://github.com/nestjs/nest/blob/master/LICENSE">MIT licensed</a>. 

![European Union’s Horizon 2020 research and innovation programme under grant agreement No 842009](NIVA.jpg)
