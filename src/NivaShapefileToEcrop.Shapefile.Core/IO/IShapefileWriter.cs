/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using NivaShapefileToEcrop.Shapefiles.Core.IO.Streams;
using NivaShapefileToEcrop.Shapefiles.Core.Models;

namespace NivaShapefileToEcrop.Shapefiles.Core.IO
{
	public interface IShapefileWriter
	{
		void AddColumn(ShapefileColumn column);
		void AddFeature(ShapefileFeature feature);
		void WriteTo(IStreamProviderRegistry registry);

		/// <summary>
		/// Writes all shapefile files to a directory.
		/// </summary>
		/// <param name="filename">Full file name to .shp file.</param>
		void WriteTo(string filename);
	}
}
