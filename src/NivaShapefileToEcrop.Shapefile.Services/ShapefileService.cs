﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System;
using System.Collections.Generic;
using NivaShapefileToEcrop.Shapefiles.Core;
using NivaShapefileToEcrop.Shapefiles.Core.Config;
using NivaShapefileToEcrop.Shapefiles.Core.IO;
using NivaShapefileToEcrop.Shapefiles.Core.Models;
using NivaShapefileToEcrop.Shapefiles.Core.Services;
using NivaShapefileToEcrop.Shapefiles.Services.IO.Compression;

namespace NivaShapefileToEcrop.Shapefiles.Services
{
    public class ShapefileService : IShapefileService
    {
        private readonly IShapefileRepository _repository;
        private readonly ShapefileConfigManager _configManager;
        private readonly IShapefileReaderFactory _readerFactory;

        public ShapefileService(IShapefileRepository shapefileRepository, IShapefileReaderFactory readerFactory, ShapefileConfigManager configManager)
        {
            _repository = shapefileRepository;
            _readerFactory = readerFactory;
            _configManager = configManager;
        }

        public bool ShapefileExists(int id)
        {
            return _repository.ShapefileExists(id);
        }

        public Shapefile GetShapefile(int id)
        {
            var shapefile = _repository.GetShapefile(id);
            return shapefile;
        }

        /// <returns>Null if shapefile does not exit.</returns>
        public IEnumerable<ShapefileFeature> GetShapefileFeatures(int id)
        {
            var shapefile = GetShapefile(id);
            if (shapefile == null)
                return null;

            var features = new List<ShapefileFeature>();

            EnumerateShapefiles(shapefile, reader =>
            {
                features.AddRange(reader.ReadFeatures());
            });

            return features;
        }

        /// <returns>Null if shapefile does not exit.</returns>
        public IEnumerable<ShapefileGeometry> GetShapefileGeometries(int id)
        {
            var shapefile = GetShapefile(id);
            if (shapefile == null)
                return null;

            var geometries = new List<ShapefileGeometry>();

            var stream = _repository.GetFileData(shapefile.Id);

            using (var zipReader = new ShapefileZipReader(stream))
            {
                while (zipReader.Read())
                {
                    var columns = ReadColumns(zipReader, shapefile);
                    var config = _configManager.FindBestMatchOrDefault(columns, shapefile.SRID);
                    var reader = zipReader.CreateReader(_readerFactory, config);
                    geometries.AddRange(reader.ReadGeometries());
                }
            }

            return geometries;
        }

        public void EnumerateShapefiles(Shapefile shapefile, Action<IShapefileReader> action)
        {
            var stream = _repository.GetFileData(shapefile.Id);

            using (var zipReader = new ShapefileZipReader(stream))
            {
                while (zipReader.Read())
                {
                    var columns = ReadColumns(zipReader, shapefile);
                    var config = _configManager.FindBestMatchOrDefault(columns);
                    var reader = zipReader.CreateReader(_readerFactory, config);
                    action(reader);
                }
            }
        }

        /// <summary>
        /// Read columns using the default configuration.
        /// </summary>
        public IEnumerable<ShapefileColumn> ReadColumns(ShapefileZipReader zipReader, Shapefile shapefile)
        {
            var config = new DefaultShapefileConfig(shapefile.SRID);
            var reader = zipReader.CreateReader(_readerFactory, config);
            return reader.ReadColumns();
        }
    }
    
}