﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using Newtonsoft.Json;

namespace Niva.Common.Models.ECropReportMessage
{
	/// <summary>
	/// A field with one or more cultivated plants or produce from one or more botanical species
	/// or varieties.
	/// </summary>
	public partial class GrownFieldCrop
	{
		/// <summary>
		/// Array containing crop production agricultural processes.
		/// </summary>
		[JsonProperty("ApplicableCropProductionAgriculturalProcess", Required = Required.Always)]
		public ApplicableCropProductionAgriculturalProcess[] ApplicableCropProductionAgriculturalProcess { get; set; }

		/// <summary>
		/// A code specifying a classification for this field crop.
		/// </summary>
		[JsonProperty("ClassificationCode", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
		public string ClassificationCode { get; set; }

		/// <summary>
		/// The name of the legislation crop
		/// </summary>
		[JsonProperty("ClassificationName")] //todo: not been decided yet
		public string ClassificationName { get; set; }

		/// <summary>
		/// An array of specified field crop mixture constituents.
		/// </summary>
		[JsonProperty("SpecifiedFieldCropMixtureConstituent", Required = Required.Always)]
		public SpecifiedFieldCropMixtureConstituent[] SpecifiedFieldCropMixtureConstituent { get; set; }
	}
}