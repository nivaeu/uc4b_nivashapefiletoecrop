/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System;
using System.Collections.Generic;
using System.Linq;
using NivaShapefileToEcrop.Shapefiles.Core.Models;

namespace NivaShapefileToEcrop.Shapefiles.Core.Config
{
	public class ShapefileConfigManager
	{
		private readonly IDictionary<string, Func<IShapefileConfig>> _configs;
		
		public ShapefileConfigManager()
		{
			_configs = new Dictionary<string, Func<IShapefileConfig>>();
		}
		
		public void Register(string name, Func<IShapefileConfig> factory)
		{
			if (string.IsNullOrEmpty(name))
				throw new ArgumentException("Name can not be null.", nameof(name));

			_configs[name] = factory;
		}
		
		public IShapefileConfig Get(string name)
		{
			if (_configs.TryGetValue(name.ToLowerInvariant(), out var factory))
				return factory();
			return null;
		}
		
		/// <summary>
		/// Returns the best match found or the default configuration if no matches were found.
		/// </summary>
		public IShapefileConfig FindBestMatchOrDefault(IEnumerable<ShapefileColumn> columns)
		{
			var match = FindBestMatch(columns, 1).FirstOrDefault();
			return match ?? new DefaultShapefileConfig();
		}

		public IShapefileConfig FindBestMatchOrDefault(IEnumerable<ShapefileColumn> columns, int srid)
		{
			var match = FindBestMatch(columns, 1).FirstOrDefault();
			var config = match ?? new DefaultShapefileConfig();
			config.SRID = srid;
			return config;
		}

		public IEnumerable<IShapefileConfig> FindBestMatch(IEnumerable<ShapefileColumn> columns, int threshold)
		{
			var matches = new SortedList<int, IShapefileConfig>();
			var comparer = new ShapefileColumnNameComparer();

			foreach(var factory in _configs.Values)
			{
				var config = factory();
				var count = config
					.GetWellKnownColumns()
					.Count(c => columns.Contains(c, comparer));

				if(count >= threshold)
					matches.Add(count, config);
			}

			return matches.Values;
		}

		internal class ShapefileColumnNameComparer : IEqualityComparer<ShapefileColumn>
		{
			public bool Equals(ShapefileColumn x, ShapefileColumn y)
			{
				return string.Equals(x?.Name, y?.Name, StringComparison.InvariantCultureIgnoreCase);
			}

			public int GetHashCode(ShapefileColumn obj)
			{
				return obj.Name.GetHashCode();
			}
		}
	}
}
