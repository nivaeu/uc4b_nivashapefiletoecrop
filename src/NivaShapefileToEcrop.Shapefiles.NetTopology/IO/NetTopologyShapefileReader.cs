/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System;
using System.Collections.Generic;
using System.Linq;
using GeoAPI.Geometries;
using NetTopologySuite.Geometries;
using NetTopologySuite.IO;
using NivaShapefileToEcrop.Shapefiles.Core.Config;
using NivaShapefileToEcrop.Shapefiles.Core.IO;
using NivaShapefileToEcrop.Shapefiles.Core.Models;
using NivaShapefileToEcrop.Shapefiles.Core.Validation;
using NTS = NetTopologySuite.IO.Streams;

namespace NivaShapefileToEcrop.Shapefiles.NetTopology.IO
{
	public class NetTopologyShapefileReader : IShapefileReader
	{
		private readonly NTS.IStreamProviderRegistry _registry;
		private IShapefileConfig _config;
		private readonly IGeometryFactory _geometryFactory;

		public IShapefileConfig Config
		{
			get => _config ?? new DefaultShapefileConfig();
			set => _config = value ?? new DefaultShapefileConfig();
		}
		
		public NetTopologyShapefileReader(NTS.IStreamProviderRegistry registry, IShapefileConfig config)
		{
			_registry = registry ?? throw new ArgumentException("Registry can not be null.", nameof(registry));
			_config = config ?? throw new ArgumentException("Config can not be null.", nameof(config));

			_geometryFactory = new GeometryFactory(new PrecisionModel(), config.SRID);
		}
		
		public ShapefileDataReader CreateReader()
		{
			return new ShapefileDataReader(_registry, _geometryFactory);
		}

		public IEnumerable<ShapefileGeometry> ReadGeometries()
		{
			using(var reader = CreateReader())
			{
				var geometries = ReadGeometries(reader)
				.Select(g => CreateShapefileGeometry(g))
				.ToArray();

				return geometries;
			}
		}

		public IEnumerable<IGeometry> ReadGeometries(ShapefileDataReader reader)
		{
			var geometries = new List<IGeometry>();

			while (reader.Read())
			{
				var geometry = ReadGeometry(reader);
				geometries.Add(geometry);
				reader.NextResult();
			}

			return geometries;
		}

		public IEnumerable<ShapefileFeature> ReadFeatures()
		{
			using (var reader = CreateReader())
			{
				var features = new List<ShapefileFeature>();

				while (reader.Read())
				{
					var feature = new ShapefileFeature();

					// Read geometry
					var geometry = ReadGeometry(reader);

					// Read attributes
					var attributes = ReadAttributes(reader);

					feature.SRID = geometry.SRID;
					feature.WellKnownText = geometry.AsText();
					feature.Attributes = attributes.ToArray();

					features.Add(feature);
				}

				return features;
			}
		}

		public IEnumerable<ShapefileAttribute> ReadAttributes(ShapefileDataReader reader)
		{
			var attributes = new List<ShapefileAttribute>();

			for (var i = 0; i < reader.DbaseHeader.NumFields; i++)
			{
				var attribute = ReadAttribute(reader, i);
				attributes.Add(attribute);
			}

			return attributes;
		}
		
		public ShapefileAttribute ReadAttribute(ShapefileDataReader reader, int index)
		{
			var field = reader.DbaseHeader.Fields[index];

			var attribute = _config.Map(new ShapefileAttribute
			{
				Name = field.Name,
				Value = reader.GetValue(index).ToString()
			});

			if(!_config.Validate(attribute))
				throw new ShapefileValidationException($"Shapefile attribute {field.Name} failed to validate.");

			return attribute;
		}
		
		/// <summary>
		/// Reads the geometry at the current record in the data reader.
		/// Coordinates are converted to WGS84 (latitude longitude).
		/// </summary>
		internal IGeometry ReadGeometry(ShapefileDataReader reader)
		{
			// Validate geometry
			if (!reader.Geometry.IsValid)
				throw new ArgumentException("Invalid geometry.");

			// Get geometry and invert shape if needed
			var geometry = ConvertGeometryToCcw(reader.Geometry);
			if (geometry.SRID == 0)
				geometry.SRID = _config.SRID;
			
			// Set coordinate z to NaN to remove z values.
			SetCoordinateZ(geometry, double.NaN);

			// If srid is not wgs84 then convert, otherwise return
			//return geometry.SRID != SpatialReferenceIds.LatLon ?
			//	GeoUtil.UTMToLatLong(geometry) : geometry; //todo: remove in first iteration
            return geometry;
        }
		
		public static void SetCoordinateZ(IEnumerable<Coordinate> coordinates, double value)
		{
			foreach (var coords in coordinates)
				coords.Z = value;
		}

		public static void SetCoordinateZ(IGeometry geometry, double value)
		{
			SetCoordinateZ(geometry.Coordinates, value);
		}

		public static ShapefileGeometry CreateShapefileGeometry(IGeometry geometry)
		{
			return new ShapefileGeometry
			{
				SRID = geometry.SRID,
				Text = geometry.AsText()
			};
		}

		public static ShapefileFeature CreateShapefileFeature(IGeometry geometry)
		{
			var feature = new ShapefileFeature
			{
				SRID = geometry.SRID,
				WellKnownText = geometry.AsText()
			};

			return feature;
		}

		public IEnumerable<ShapefileColumn> ReadColumns()
		{
			using (var reader = CreateReader())
			{
				var header = reader.DbaseHeader;

				var columns = header.Fields
					.Select(f => new ShapefileColumn
					{
						Name = f.Name,
						Length = f.Length,
						Type = f.DbaseType,
						DecimalCount = f.DecimalCount
					})
					.ToArray();

				return columns;
			}
		}

        /// <summary>
        /// Converts CW IGeometry to CCW IGeometry. 
        /// </summary>
        /// <param name="geometry"></param>
        /// <returns></returns>
        private static IGeometry ConvertGeometryToCcw(IGeometry geometry)
        {
            if (geometry is IPolygon polygon && !polygon.Shell.IsCCW)
            {
                return geometry.Reverse();
            }

            if (geometry is IMultiPolygon multiPolygon)
            {
                for (int i = 0; i < multiPolygon.Geometries.Length; i++)
                {
                    if (multiPolygon.Geometries[i] is IPolygon geom)
                    {
                        if (!geom.Shell.IsCCW)
                        {
                            multiPolygon.Geometries[i] = geom.Reverse();
                        }
                    }
                    else
                    {
                        throw new ArgumentException("Multipolygon does not only contain Polygon");
                    }
                }
            }

            return geometry;
        }
    }
}
