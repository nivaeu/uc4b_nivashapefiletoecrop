﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System.IO;
using Niva.Common.Models.ECropReportMessage;

namespace NivaShapefileToEcrop.Mapper
{
    public class PartialECropShapefileInput
    {
        public EcropMessage ECropMessage { get; set; }
        public string Id { get; set; }
        public Stream ZipShapefile { get; set; }
        public string UnitInShapefile { get; set; }
        public string ColumnName { get; set; }
    }
}