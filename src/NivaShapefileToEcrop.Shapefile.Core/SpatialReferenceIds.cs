﻿namespace NivaShapefileToEcrop.Shapefiles.Core
{
    public static class SpatialReferenceIds
    {
        public const int ETRS89UtmZone32 = 3044;
        public const int WebMercator = 3395;    // Based on Wgs84
        public const int PseudoMercator = 3857; // Based on Wgs84
        public const int LatLon = 4326;
        public const int Utm32N = 32632;
    }
}
