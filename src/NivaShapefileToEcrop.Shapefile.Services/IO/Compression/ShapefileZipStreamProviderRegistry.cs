/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System;
using System.IO;
using System.IO.Compression;
using System.Linq;
using NivaShapefileToEcrop.Shapefiles.Core.IO;
using NivaShapefileToEcrop.Shapefiles.Core.IO.Streams;

namespace NivaShapefileToEcrop.Shapefiles.Services.IO.Compression
{
	public class ShapefileZipStreamProviderRegistry : IStreamProviderRegistry
	{
		private readonly ZipArchive _archive;
		private int _index;

		public bool IsReadOnly { get; }
		
		public ShapefileZipStreamProviderRegistry(ZipArchive archive, bool isReadOnly) : this(archive, -1, isReadOnly)
		{

		}
		
		public ShapefileZipStreamProviderRegistry(ZipArchive archive, int index, bool isReadOnly)
		{
			_archive = archive;
			_index = index;
			IsReadOnly = isReadOnly;
		}

		/// <summary>
		/// Register a new archive entry.
		/// </summary>
		public void Register(string filename)
		{
			if (IsReadOnly)
				throw new NotSupportedException("Registry is read only.");

			_archive.CreateEntry(filename);
		}

		/// <summary>
		/// Moves index to the first shapefile in the archive.
		/// </summary>
		/// <returns>False if no shapefile was found.</returns>
		public bool MoveToFirstShapefile()
		{
			_index = FindIndex(0, e => e.Name.EndsWith(ShapefileFileExtensions.Main));
			return IsIndexValid();
		}
		
		public int FindIndex(int startIndex, Func<ZipArchiveEntry, bool> predicate)
		{
			for (var i = startIndex; i < _archive.Entries.Count; i++)
				if (predicate(_archive.Entries[i]))
					return i;
			return -1;
		}

		/// <summary>
		/// Check if the current index is within range.
		/// </summary>
		public bool IsIndexValid()
		{
			return _index >= 0 && _index < _archive.Entries.Count;
		}
		
		public ZipArchiveEntry GetCurrentEntry()
		{
			return IsIndexValid() ? _archive.Entries[_index] : null;
		}
		
		public ZipArchiveEntry GetCurrentEntry(string extension)
		{
			var entry = GetCurrentEntry();
			if (entry == null)
				return null;

			var name = Path.GetFileNameWithoutExtension(entry.Name) + extension;
			return _archive.Entries
				.FirstOrDefault(e => string.Equals(e.Name, name, StringComparison.InvariantCultureIgnoreCase));
		}

		public IStreamProvider GetProvider(string key)
		{
			// Get current entry or move to first
			var entry = GetCurrentEntry();
			if (entry == null)
			{
				MoveToFirstShapefile();
				entry = GetCurrentEntry();
			}

			if (entry == null)
				return null;

			entry = GetCurrentEntry(key);
			if (entry == null)
				return null;

			var provider = new ShapefileZipStreamProvider(entry, IsReadOnly);
			return provider;
		}
	}
}
