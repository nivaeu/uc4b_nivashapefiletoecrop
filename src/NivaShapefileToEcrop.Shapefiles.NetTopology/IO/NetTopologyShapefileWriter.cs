/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeoAPI.Geometries;
using NetTopologySuite.Features;
using NetTopologySuite.Geometries;
using NetTopologySuite.IO;
using NivaShapefileToEcrop.Shapefiles.Core;
using NivaShapefileToEcrop.Shapefiles.Core.IO;
using NivaShapefileToEcrop.Shapefiles.Core.IO.Streams;
using NivaShapefileToEcrop.Shapefiles.Core.Models;
using NivaShapefileToEcrop.Shapefiles.NetTopology.IO.Streams;

namespace NivaShapefileToEcrop.Shapefiles.NetTopology.IO
{
	public class NetTopologyShapefileWriter : IShapefileWriter
	{
		private readonly IGeometryFactory _geometryFactory;
		private readonly IList<IFeature> _features;
		private readonly IList<ShapefileColumn> _columns;
		private readonly Encoding _encoding;

		public NetTopologyShapefileWriter() : this(SpatialReferenceIds.LatLon, Encoding.UTF8)
		{

		}

		public NetTopologyShapefileWriter(int srid) : this(srid, Encoding.UTF8)
		{

		}

		public NetTopologyShapefileWriter(int srid, Encoding encoding)
		{
			_geometryFactory = new GeometryFactory(new PrecisionModel(), srid);
			_features = new List<IFeature>();
			_columns = new List<ShapefileColumn>();
			_encoding = encoding;
		}

		public void AddFeature(ShapefileFeature source)
		{
			var reader = new WKTReader(_geometryFactory);
			var geometry = reader.Read(source.WellKnownText);
			var attributes = CreateAttributeTable(source.Attributes);
			var feature = new Feature(geometry, attributes);
			
			_features.Add(feature);
		}

		public void AddColumn(ShapefileColumn column)
		{
			_columns.Add(column);
		}
		
		public IAttributesTable CreateAttributeTable(IEnumerable<ShapefileAttribute> attributes)
		{
			var table = new AttributesTable();

			if (attributes != null)
				foreach (var att in attributes)
					table.AddAttribute(att.Name, att.Value);

			return table;
		}

		public DbaseFileHeader CreateHeader()
		{
			var header = new DbaseFileHeader(_encoding);
			foreach (var column in _columns)
				header.AddColumn(column.Name, column.Type, column.Length, column.DecimalCount);
			header.NumRecords = _features.Count;
			
			return header;
		}
		
		public bool ColumnExists(string name)
		{
			return _columns.Count(c => c.Name == name) == 1;
		}

		public void WriteTo(string filename)
		{
			var name = Path.GetFileNameWithoutExtension(filename);
			var path = Path.GetDirectoryName(filename);
			WriteTo(path, name);
		}

		/// <summary>
		/// Writes shapefile to a directory.
		/// </summary>
		/// <param name="path">Directory path, will be created if it doesn't exist.</param>
		/// <param name="name">Name of shapefile without file extension.</param>
		public void WriteTo(string path, string name)
		{
			var dir = new DirectoryInfo(path);
			dir.Create();

			var shapeFile = new FileInfo(Path.Combine(dir.FullName, name + ShapefileFileExtensions.Main));

			var header = CreateHeader();
			var writer = new ShapefileDataWriter(shapeFile.FullName, _geometryFactory, _encoding)
			{
				Header = header
			};
			
			writer.Write(_features);
		}

		public void WriteTo(IStreamProviderRegistry registry)
		{
			if (registry.IsReadOnly)
				throw new ArgumentException("Registry is read only.", nameof(registry));

			var ntsRegistry = new NetTopologyStreamProviderRegistry(registry);
			var header = CreateHeader();
			var writer = new ShapefileDataWriter(ntsRegistry, _geometryFactory, _encoding)
			{
				Header = header
			};

			writer.Write(_features);
		}

		public Task WriteToAsync(IStreamProviderRegistry registry)
		{
			WriteTo(registry);
			return Task.CompletedTask;
		}
	}
}
