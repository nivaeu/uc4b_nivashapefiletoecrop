/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

namespace NivaShapefileToEcrop.Shapefiles.Core.Models
{
	public class ShapefileAttribute
	{
		public string Name { get; set; }
		public string Value { get; set; }

		public ShapefileAttribute Copy()
		{
			return new ShapefileAttribute
			{
				Name = Name,
				Value = Value
			};
		}
	}
}