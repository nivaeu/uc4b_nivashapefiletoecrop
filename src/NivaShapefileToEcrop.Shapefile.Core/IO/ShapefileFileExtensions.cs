/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

namespace NivaShapefileToEcrop.Shapefiles.Core.IO
{
	/// <summary>
	/// Shapefile file extensions and their descriptions as described on https://www.loc.gov/preservation/digital/formats/fdd/fdd000280.shtml.
	/// </summary>
	public static class ShapefileFileExtensions
	{
		/// <summary>
		/// Required Main file.
		/// </summary>
		public const string Main = ".shp";

		/// <summary>
		/// Index file (mandatory). In the index file, each record contains the offset of the corresponding main file record from the beginning of the main file. The index file (.shx) contains a 100-byte header followed by 8-byte, fixed-length records.
		/// </summary>
		public const string Index = ".shx";

		/// <summary>
		/// dBASE Table file (mandatory); a constrained form of Dbf that contains feature attributes with one record per feature. The one-to-one relationship between geometry and attributes is based on record number. Attribute records in the dBASE file must be in the same order as records in the main file.
		/// </summary>
		public const string Dbf = ".dbf";

		/// <summary>
		/// Part 1 of spatial index for read-write instances of the Shapefile format. If present, essential for correct processing.
		/// </summary>
		public const string SpatialIndexFirstPart = ".sbn";

		/// <summary>
		/// Part 2 of spatial index for read-write instances of the Shapefile format. If present, essential for correct processing.
		/// </summary>
		public const string SpatialIndexSecondPart = ".sbx";

		/// <summary>
		/// One of the files that store the spatial index of the features for instances of the Shapefile format that are read-only.
		/// </summary>
		public const string FeatureIndex = ".fbn";

		/// <summary>
		/// The other file (besides .fbn) that stores the spatial index of the features for instances of the Shapefile format that are read-only. 
		/// </summary>
		public const string AlternativeFeatureIndex = ".fbx";

		/// <summary>
		/// One of the files that stores the attribute index of the active fields in a table or a theme's attribute table.
		/// </summary>
		public const string AttributeIndex = ".ain";

		/// <summary>
		/// The other file (besides .ain) that stores the attribute index of the active fields in a table or a theme's attribute table
		/// </summary>
		public const string AlternativeAttributeIndex = ".aih";

		/// <summary>
		/// Geocoding index for read/write shapefiles. If present, essential for correct processing.
		/// </summary>
		public const string GeocodingIndex = ".ixs";

		/// <summary>
		/// Geocoding index for read-write shapefiles (ODB format).
		/// </summary>
		public const string ODBGeocodingIndex = ".mxs";

		/// <summary>
		/// Projections Definition file; stores coordinate system information.
		/// </summary>
		public const string Projections = ".prj";

		/// <summary>
		/// Contains metadata, as used by ArcGIS.
		/// </summary>
		public const string ArcGISMetadata = ".xml";

		/// <summary>
		/// An optional file that can be used to specify the codepage for identifying the character set to be used.
		/// </summary>
		public const string Codebase = ".cpg";
	}
}
