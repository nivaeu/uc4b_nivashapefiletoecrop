/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System.Collections.Generic;
using NivaShapefileToEcrop.Shapefiles.Core.Models;

namespace NivaShapefileToEcrop.Shapefiles.Core.Config
{
	public interface IShapefileConfig
	{
		string Name { get; }
		int SRID { get; set; }

		IEnumerable<ShapefileColumn> GetWellKnownColumns();

		/// <summary>
		/// Maps an attribute if a data map is found and returns a new update attribute.
		/// </summary>
		ShapefileAttribute Map(ShapefileAttribute attribute);

		/// <summary>
		/// Validates an attribute if a validator has been configured.
		/// </summary>
		/// <exception cref="ShapefileValidationException"></exception>
		/// <returns>True on successful validation or no validation.</returns>
		bool Validate(ShapefileAttribute attribute);
	}
}