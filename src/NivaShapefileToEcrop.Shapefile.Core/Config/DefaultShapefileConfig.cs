/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System;
using NivaShapefileToEcrop.Shapefiles.Core.Models;

namespace NivaShapefileToEcrop.Shapefiles.Core.Config
{
	/// <summary>
	/// Default shapefile config implementation that matches all attributes with no specific rules.
	/// </summary>
	public class DefaultShapefileConfig : ShapefileConfig
	{
		/// <summary>
		/// Default configuration with srid as WGS84.
		/// </summary>
		public DefaultShapefileConfig() : this(4326)
		{

		}

		public DefaultShapefileConfig(int srid)
		{
			Name = "Default";
			SRID = srid;

			DataMaps = Array.Empty<ShapefileDataMap>();
			WellKnownColumns = new[]
			{
				new ShapefileColumn
				{
					Name = WellKnownAttributes.Quantity,
					Type = ShapefileColumnTypes.Number,
					Length = 8
				}
			};
		}
	}
}