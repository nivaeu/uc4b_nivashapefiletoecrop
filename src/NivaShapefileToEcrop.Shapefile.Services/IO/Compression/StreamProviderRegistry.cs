/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System.Collections.Generic;
using NivaShapefileToEcrop.Shapefiles.Core.IO.Streams;

namespace NivaShapefileToEcrop.Shapefiles.Services.IO.Compression
{
	public class StreamProviderRegistry : IStreamProviderRegistry
	{
		private readonly IDictionary<string, IStreamProvider> _providers;

		public StreamProviderRegistry(bool isReadOnly)
		{
			_providers = new Dictionary<string, IStreamProvider>();
			IsReadOnly = isReadOnly;
		}

		public bool IsReadOnly { get; }

		public void Register(string name, IStreamProvider provider)
		{
			_providers[name.ToLowerInvariant()] = provider;
		}

		public IStreamProvider GetProvider(string key)
		{
			if (_providers.TryGetValue(key.ToLowerInvariant(), out IStreamProvider provider))
				return provider;
			return null;
		}
	}
}
