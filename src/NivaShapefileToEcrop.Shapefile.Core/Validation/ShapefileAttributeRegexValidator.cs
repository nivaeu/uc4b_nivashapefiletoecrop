/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System.Text.RegularExpressions;
using NivaShapefileToEcrop.Shapefiles.Core.Models;

namespace NivaShapefileToEcrop.Shapefiles.Core.Validation
{
	public class ShapefileAttributeRegexValidator : IShapefileAttributeValidator
	{
		public const string Number = @"/^[+-]?([0-9]+\.?[0-9]*|\.[0-9]+)$/";

		private readonly Regex _regex;

		public ShapefileAttributeRegexValidator(string regex)
		{
			_regex = new Regex(regex);
		}

		public ShapefileAttributeRegexValidator(Regex regex)
		{
			_regex = regex;
		}

		public bool Validate(ShapefileAttribute attribute)
		{
			if (string.IsNullOrEmpty(_regex.ToString()))
				return true;

			return _regex.IsMatch(attribute.Value);
		}
	}
}
