/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System;
using System.IO;

namespace NivaShapefileToEcrop.Shapefiles.Core.IO.Streams
{
	public class ObservableMemoryStream : MemoryStream
	{
		public event Action<ObservableMemoryStream> Closing;
		public event Action<ObservableMemoryStream> Closed;
		public event Action<ObservableMemoryStream> Disposing;
		public event Action<ObservableMemoryStream> Disposed;
		public event Action<ObservableMemoryStream> LengthChanged;
		public event Action<ObservableMemoryStream> Flushed;
		
		public ObservableMemoryStream(byte[] buffer) : base(buffer)
		{
			
		}

		public ObservableMemoryStream(byte[] buffer, bool writable) : base(buffer, writable)
		{

		}

		public override void Flush()
		{
			base.Flush();
			Flushed?.Invoke(this);
		}
		
		public override void SetLength(long value)
		{
			base.SetLength(value);
			LengthChanged?.Invoke(this);
		}
		
		protected override void Dispose(bool disposing)
		{
			Disposing?.Invoke(this);
			base.Dispose(disposing);
			Disposed?.Invoke(this);
		}

		public override void Close()
		{
			Closing?.Invoke(this);
			base.Close();
			Closed?.Invoke(this);
		}
	}
}
