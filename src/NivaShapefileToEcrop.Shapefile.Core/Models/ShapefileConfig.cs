/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System;
using System.Collections.Generic;
using System.Linq;
using NivaShapefileToEcrop.Shapefiles.Core.Config;
using NivaShapefileToEcrop.Shapefiles.Core.Validation;

namespace NivaShapefileToEcrop.Shapefiles.Core.Models
{
	public class ShapefileConfig : IShapefileConfig
	{
		public string Name { get; set; }
		public int SRID { get; set; }
		public ShapefileDataMap[] DataMaps { get; set; }
		public ShapefileColumn[] WellKnownColumns { get; set; }

		public ShapefileDataMap GetDataMap(string column)
		{
			return DataMaps
				.FirstOrDefault(map => string.Equals(column, map.Column, StringComparison.InvariantCultureIgnoreCase));
		}

		public IEnumerable<ShapefileDataMap> GetDataMaps()
		{
			return DataMaps;
		}

		public IEnumerable<ShapefileColumn> GetWellKnownColumns()
		{
			return WellKnownColumns ?? Array.Empty<ShapefileColumn>();
		}

		public ShapefileAttribute Map(ShapefileAttribute attribute)
		{
			var map = GetDataMap(attribute.Name);
			if (map == null)
				return attribute;

			var copy = attribute.Copy();
			copy.Name = map.Name;

			return copy;
		}

		public bool Validate(ShapefileAttribute attribute)
		{
			var map = GetDataMap(attribute.Name);
			if (map == null)
				return true;

			var validator = GetAttributeValidator(map.Validation);
			return validator.Validate(attribute);
		}

		public IShapefileAttributeValidator GetAttributeValidator(string key)
		{
			switch (key?.ToLowerInvariant())
			{
				case "number":
					return new ShapefileAttributeRegexValidator(ShapefileAttributeRegexValidator.Number);
				default:
					return new ShapefileAttributeRegexValidator(key);
			}
		}
	}
}