/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using NivaShapefileToEcrop.Shapefiles.Core.Config;
using NivaShapefileToEcrop.Shapefiles.Core.IO;
using NivaShapefileToEcrop.Shapefiles.Core.IO.Streams;
using NivaShapefileToEcrop.Shapefiles.NetTopology.IO.Streams;

namespace NivaShapefileToEcrop.Shapefiles.NetTopology.IO
{
	public class NetTopologyShapefileReaderFactory : IShapefileReaderFactory
	{
		public IShapefileReader Create(IStreamProviderRegistry registry, IShapefileConfig config)
		{
			var ntsRegistry = new NetTopologyStreamProviderRegistry(registry);
			return new NetTopologyShapefileReader(ntsRegistry, config);
		}
	}
}
