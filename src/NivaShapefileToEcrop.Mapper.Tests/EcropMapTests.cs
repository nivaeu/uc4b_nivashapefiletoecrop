﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System.IO;
using FluentAssertions;
using Newtonsoft.Json;
using Niva.Common.Models.ECropReportMessage;
using NivaShapefileToEcrop.Shapefiles.NetTopology.IO;
using NUnit.Framework;

namespace NivaShapefileToEcrop.Mapper.Tests
{
    internal class EcropMapTests
    {
        [Test]
        public void Can()
        {
            //Arrange
            var ecropJson = File.ReadAllText("TestData/partialEcropMessage.json");
            var partialECropMessage = JsonConvert.DeserializeObject<EcropMessage>(ecropJson);
            var zipShapefile = new MemoryStream(File.ReadAllBytes("TestData/Export_20210819_1419.zip"));
            var reader = new NetTopologyShapefileReaderFactory();
            var input = new PartialECropShapefileInput
            {
                ZipShapefile = zipShapefile,
                ECropMessage = partialECropMessage,
                Id = "54_Application_2021-07-29_00",
                UnitInShapefile = "kg/ha",
                ColumnName = "AppliedRate"
            };
            var sut = new ECropMapper(reader);

            //Act
            var result = sut.Map(input);
            
            //Assert
            result.Should().NotBeNull();
            result.ECROPReportMessage.Should().NotBeNull();
            result.ECROPReportMessage.AgriculturalProducerParty.AgriculturalProductionUnit
                .SpecifiedCropPlot[0].GrownFieldCrop[0].ApplicableCropProductionAgriculturalProcess[0]
                .AppliedSpecifiedAgriculturalApplication[0].AppliedSpecifiedAgriculturalApplicationRate.Length.Should()
                .Be(7884);
        }
    }
}
