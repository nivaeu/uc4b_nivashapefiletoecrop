/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System;
using System.Collections.Generic;

namespace NivaShapefileToEcrop.Shapefiles.Core.IO.Streams
{
	public class StreamProviderRegistry : IStreamProviderRegistry
	{
		private readonly Dictionary<string, StreamProviderFactory> _providers;

		public bool IsReadOnly { get; }

		public StreamProviderRegistry(bool isReadOnly)
		{
			_providers = new Dictionary<string, StreamProviderFactory>();
			IsReadOnly = isReadOnly;
		}

		public IStreamProvider GetProvider(string key)
		{
			return _providers.TryGetValue(key, out var provider) ? provider.Get() : null;
		}

		public void Register(string key, IStreamProvider provider)
		{
			_providers[key] = new StreamProviderFactory(provider);
		}

		public void Register(string key, Func<IStreamProvider> factory)
		{
			_providers[key] = new StreamProviderFactory(factory);
		}

		class StreamProviderFactory
		{
			private readonly Func<IStreamProvider> _factory;

			public StreamProviderFactory(IStreamProvider provider)
			{
				_factory = () => provider;
			}

			public StreamProviderFactory(Func<IStreamProvider> factory)
			{
				_factory = factory;
			}

			public IStreamProvider Get()
			{
				return _factory();
			}
		}
	}
}
