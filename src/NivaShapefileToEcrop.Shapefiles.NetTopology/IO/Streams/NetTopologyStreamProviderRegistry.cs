/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System;
using NivaShapefileToEcrop.Shapefiles.Core.IO.Streams;
using NTS = NetTopologySuite.IO.Streams;

namespace NivaShapefileToEcrop.Shapefiles.NetTopology.IO.Streams
{
	public class NetTopologyStreamProviderRegistry : NTS.IStreamProviderRegistry
	{
		private readonly IStreamProviderRegistry _registry;

		public NetTopologyStreamProviderRegistry(IStreamProviderRegistry registry)
		{
			_registry = registry;
		}

		public bool IsReadOnly => _registry.IsReadOnly;

		public NTS.IStreamProvider this[string streamType] => GetStreamProvider(streamType);

		public NTS.IStreamProvider GetStreamProvider(string streamType)
		{
			var extension = NetTopologyShapefileExtensionsMap.ToExtension(streamType);
			var provider = _registry.GetProvider(extension);

			if (provider == null)
				throw new Exception($"Stream provider for extension {extension} not found.");

			return new NetTopologyStreamProvider(provider, streamType, IsReadOnly);
		}
	}
}
