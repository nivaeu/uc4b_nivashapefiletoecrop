/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using NivaShapefileToEcrop.Shapefiles.Core.Config;
using NivaShapefileToEcrop.Shapefiles.Core.IO;
using NivaShapefileToEcrop.Shapefiles.Core.IO.Streams;

namespace NivaShapefileToEcrop.Shapefiles.Services.IO.Compression
{
	/// <summary>
	/// Shapefile zip reader that works in the file system to process the zip archive.
	/// </summary>
	public class ShapefileZipReader : IDisposable
	{
		private readonly ZipArchive _archive;
		private int _index = -1;

		public ShapefileZipReader(string filename)
		{
			var stream = File.OpenRead(filename);
			_archive = new ZipArchive(stream);
		}

		public ShapefileZipReader(byte[] bytes)
		{
			var stream = new MemoryStream(bytes);
			_archive = new ZipArchive(stream);
		}

		public ShapefileZipReader(Stream stream)
		{
			_archive = new ZipArchive(stream);
		}

		/// <summary>
		/// Current entry index in the archive.
		/// </summary>
		public int Index => _index;

		/// <summary>
		/// Advances to the next available shapefile in the archive.
		/// </summary>
		/// <returns>False if there no more files.</returns>
		public bool Read()
		{
			_index = FindNextShapefile(_index + 1);
			return IsCurrentEntryShapefile();
		}

		public void Reset()
		{
			_index = -1;
		}

		/// <summary>
		/// Move index to the first shapefile in the archive.
		/// </summary>
		public bool MoveToFirst()
		{
			_index = FindNextShapefile(0);
			return IsCurrentEntryShapefile();
		}

		public int FindNextShapefile(int indexOffset)
		{
			for(var i = indexOffset; i < _archive.Entries.Count; i++)
			{
				var entry = _archive.Entries[i];
				if (IsEntryShapefile(entry))
					return i;
			}

			return -1;
		}

		public string GetCurrentPath()
		{
			var entry = GetCurrentEntry();
			return entry == null ? null : Path.GetFullPath(entry.FullName);
		}

		public IEnumerable<FileInfo> ExtractArchive(DirectoryInfo directory)
		{
			var files = new List<FileInfo>();

			foreach (var entry in _archive.Entries)
				files.Add(ExtractEntry(directory, entry));

			return files;
		}

		public FileInfo ExtractEntry(DirectoryInfo directory, ZipArchiveEntry entry)
		{
			var file = new FileInfo(Path.Combine(directory.FullName, entry.Name));

			using (var fileStream = file.Open(FileMode.Create, FileAccess.Write))
				using (var entryStream = entry.Open())
				{
					entryStream.CopyTo(fileStream);
				}

			return file;
		}

		public IShapefileReader CreateReader(IShapefileReaderFactory factory, IShapefileConfig config)
		{
			var registry = CreateRegistry();
			return factory.Create(registry, config);
		}

		public IStreamProviderRegistry CreateRegistry()
		{
			if (!IsCurrentEntryShapefile())
				throw new Exception("Current entry is not a shapefile. Use Read to safely advance in archive.");

			var entry = GetCurrentEntry();
			var name = GetName();
			var directory = GetDirectoryName();
			var registry = new StreamProviderRegistry(true);

			// Register shapefile
			registry.Register(ShapefileFileExtensions.Main, new ShapefileZipStreamProvider(entry, true));

			// Register dbf
			var dbfPath = Path.Combine(directory, name + ShapefileFileExtensions.Dbf).Replace('\\', '/');
			var dbfEntry = _archive.GetEntry(dbfPath);
			registry.Register(ShapefileFileExtensions.Dbf, new ShapefileZipStreamProvider(dbfEntry, true));

			// Register index
			var indexPath = Path.Combine(directory, name + ShapefileFileExtensions.Index).Replace('\\', '/');
			var indexEntry = _archive.GetEntry(indexPath);
			registry.Register(ShapefileFileExtensions.Index, new ShapefileZipStreamProvider(indexEntry, true));

			return registry;
		}

		public void Dispose()
		{
			_archive.Dispose();
		}

		/// <summary>
		/// Get the name of the current entry without file extension.
		/// </summary>
		public string GetName()
		{
			var entry = GetCurrentEntry();
			if (entry == null)
				return null;
			return Path.GetFileNameWithoutExtension(entry.Name);
		}

		/// <summary>
		/// Get directory path for the current entry.
		/// </summary>
		public string GetDirectoryName()
		{
			var entry = GetCurrentEntry();
			return Path.GetDirectoryName(entry?.FullName);
		}

		public ZipArchiveEntry GetCurrentEntry()
		{
			if (!IsIndexValid())
				return null;

			return _archive.Entries[_index];
		}

		public bool IsIndexValid()
		{
			return _index >= 0 && _index < _archive.Entries.Count;
		}

		public bool IsCurrentEntryShapefile()
		{
			var entry = GetCurrentEntry();
			return IsEntryShapefile(entry);
		}

		public bool IsEntryShapefile(ZipArchiveEntry entry)
		{
			if (entry == null)
				return false;

			return string.Equals(
				ShapefileFileExtensions.Main,
				Path.GetExtension(entry.Name),
				StringComparison.InvariantCultureIgnoreCase
			);
		}
	}
}
