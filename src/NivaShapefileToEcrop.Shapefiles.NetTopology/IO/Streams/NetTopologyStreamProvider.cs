/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using NivaShapefileToEcrop.Shapefiles.Core.IO.Streams;
using System.IO;
using NTS = NetTopologySuite.IO.Streams;

namespace NivaShapefileToEcrop.Shapefiles.NetTopology.IO.Streams
{
	public class NetTopologyStreamProvider : NTS.IStreamProvider
	{
		private readonly IStreamProvider _provider;
		private readonly string _streamType;
		private readonly bool _isReadOnly;

		public NetTopologyStreamProvider(IStreamProvider provider, string streamType, bool isReadOnly)
		{
			_provider = provider;
			_streamType = streamType;
			_isReadOnly = isReadOnly;
		}

		public bool UnderlyingStreamIsReadonly => _isReadOnly;

		public string Kind => _streamType;

		public Stream OpenRead()
		{
			return _provider.OpenRead();
		}

		public Stream OpenWrite(bool truncate)
		{
			return _provider.OpenWrite(truncate);
		}
	}
}
