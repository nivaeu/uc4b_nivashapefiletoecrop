﻿/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using GeoAPI.Geometries;
using NetTopologySuite.Geometries;
using NetTopologySuite.IO;
using Niva.Common.Models.ECropReportMessage;
using NivaShapefileToEcrop.Shapefiles.Core;
using NivaShapefileToEcrop.Shapefiles.Core.Config;
using NivaShapefileToEcrop.Shapefiles.Core.IO;
using NivaShapefileToEcrop.Shapefiles.Core.Models;
using NivaShapefileToEcrop.Shapefiles.Services.IO.Compression;

namespace NivaShapefileToEcrop.Mapper
{
    public class ECropMapper
    {
        private readonly IShapefileReaderFactory _readerFactory;

        public ECropMapper(IShapefileReaderFactory readerFactory)
        {
            _readerFactory = readerFactory;
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance); //todo encoding need by reader for windows encoding 1252
        }

        public EcropMessage Map(PartialECropShapefileInput input)
        {
            var result = input.ECropMessage;
            var geometryFactory = new GeometryFactory(new PrecisionModel(), SpatialReferenceIds.LatLon);
            using (var zipReader = new ShapefileZipReader(input.ZipShapefile))
            {
                while (zipReader.Read())
                {
                    var name = zipReader.GetName();
                    if (!string.Equals(name, input.Id, StringComparison.CurrentCultureIgnoreCase))
                    {
                        continue;
                    }

                    //var columns = ReadColumns(zipReader);
                    var reader = zipReader.CreateReader(_readerFactory, new DefaultShapefileConfig());
                    var features = reader.ReadFeatures();
                    var rates = CreateRates(features, geometryFactory, input.UnitInShapefile, input.ColumnName).ToList();

                    var appliedSpecifiedAgriculturalApplication = result.ECROPReportMessage.AgriculturalProducerParty
                        .AgriculturalProductionUnit.SpecifiedCropPlot[0].GrownFieldCrop[0]
                        .ApplicableCropProductionAgriculturalProcess[0]
                        .AppliedSpecifiedAgriculturalApplication[0];

                    appliedSpecifiedAgriculturalApplication.AppliedSpecifiedAgriculturalApplicationRate =
                        rates.ToArray();
                    break;
                }
            }

            return result;
        }

        private IEnumerable<ShapefileColumn> ReadColumns(ShapefileZipReader zipReader)
        {
            var config = new DefaultShapefileConfig();
            var reader = zipReader.CreateReader(_readerFactory, config);
            return reader.ReadColumns();
        }

        private static IEnumerable<AppliedSpecifiedAgriculturalApplicationRate> CreateRates(
            IEnumerable<ShapefileFeature> features, IGeometryFactory geometryFactory, string unit, string columnName)
        {
            var rates = new List<AppliedSpecifiedAgriculturalApplicationRate>();

            foreach (var feature in features)
            {
                var quantity = feature.Attributes.FirstOrDefault(a => a.Name == columnName);
                if (quantity == null)
                    continue;

                var geometry = CreateGeometry(feature, geometryFactory);
                var isPolygon = string.Equals(geometry.GeometryType, "polygon", StringComparison.CurrentCultureIgnoreCase);
                Niva.Common.Models.ECropReportMessage.Geometry geom = null;
                if (isPolygon)
                {
                    geom = new PolygonGeometry
                    {
                        coordinates = ConvertUTM32GeometryToCoordsAs3DArray(geometry)
                    };

                }
                else
                {
                    geom = new PointGeometry
                    {
                        coordinates = new[] { geometry.Coordinates[0].X, geometry.Coordinates[0].Y }
                    };
                }

                var temp = new AppliedSpecifiedAgriculturalApplicationRate()
                {
                    AppliedQuantity = double.Parse(quantity.Value),
                    AppliedQuantityUnit = unit,
                    AppliedReferencedLocation = new []
                    {
                        new ReferencedLocation
                        {
                            PhysicalSpecifiedGeographicalFeature = new PhysicalSpecifiedGeographicalFeature
                            {
                                type = PhysicalSpecifiedGeographicalFeatureType.Feature,
                                geometry = geom
                            },
                            TypeCode = "TAP",
                            ReferenceTypeCode = "CAP"
                        }
                    }
                };
                rates.Add(temp);
            }

            return rates;
        }

        public static IGeometry CreateGeometry(ShapefileFeature feature, IGeometryFactory geometryFactory)
        {
            return new WKTReader(geometryFactory).Read(feature.WellKnownText);
        }

        private static double[][][] ConvertUTM32GeometryToCoordsAs3DArray(IGeometry utmGeometry)
        {
            string geometryLatLongWkt = utmGeometry.AsText();

            // Strip out everything except the coordinates
            var polygonRawText = geometryLatLongWkt.Replace("POLYGON ", "");
            polygonRawText = polygonRawText.Replace("((", "(");
            polygonRawText = polygonRawText.Replace("))", ")");
            polygonRawText = polygonRawText.Replace(", ", ",");
            polygonRawText = polygonRawText.Replace("),(", ")-(");

            string[] rings = polygonRawText.Split('-');
            //3D array consists of [rings][coordinate sets][coordinates]
            //e.g. a polygon with 1 outer ring and 2 inner rings, with each ring having 4 coordinates, has size [3][4][2]
            var coordsAs3DArray = new double[rings.Length][][];

            //Create coordsAs3DArray from linear rings
            for (var ring = 0; ring < rings.Length; ring++)
            {
                string coordRawText = rings[ring].Replace("(", "");
                coordRawText = coordRawText.Replace(")", "");
                // Seperate coordinates to iterate through
                IEnumerable<string> coordStrings = coordRawText.Split(',').Select(coord => coord.Replace(" ", ","));

                // Build list of coordinates
                var coords = new List<double[]>();
                foreach (string coord in coordStrings)
                {
                    string[] splt = coord.Split(',');
                    double x = Math.Round(double.Parse(splt[0], CultureInfo.InvariantCulture), 6);
                    double y = Math.Round(double.Parse(splt[1], CultureInfo.InvariantCulture), 6);

                    coords.Add(new[] { x, y });
                }

                coordsAs3DArray[ring] = new double[coords.Count][];

                for (var i = 0; i < coords.Count; i++)
                {
                    coordsAs3DArray[ring][i] = new double[2];

                    coordsAs3DArray[ring][i][0] = coords.ElementAt(i)[0];
                    coordsAs3DArray[ring][i][1] = coords.ElementAt(i)[1];
                }
            }

            return coordsAs3DArray;
        }
    }
}