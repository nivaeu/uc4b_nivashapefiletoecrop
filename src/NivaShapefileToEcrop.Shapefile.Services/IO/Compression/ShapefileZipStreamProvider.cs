/*
 * Copyright (c) RVO: Ministerie van Economische Zaken en Klimat NL, SEGES: Landbrug & Fodevarer F.M.B.A. DK 2019--2021.
 * This file belongs to subproject WP2 Use Case 4b of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license
*/

using System;
using System.IO;
using System.IO.Compression;
using NivaShapefileToEcrop.Shapefiles.Core.IO.Streams;

namespace NivaShapefileToEcrop.Shapefiles.Services.IO.Compression
{
	public class ShapefileZipStreamProvider : IStreamProvider
	{
		private readonly ZipArchiveEntry _entry;

		public ShapefileZipStreamProvider(ZipArchiveEntry entry) : this(entry, false)
		{

		}

		public ShapefileZipStreamProvider(ZipArchiveEntry entry, bool isReadOnly)
		{
			_entry = entry ?? throw new ArgumentException("Archive entry can not be null.", nameof(entry));
			IsReadOnly = isReadOnly;
		}

		public bool IsReadOnly { get; }

		public Stream OpenRead()
		{
			return OpenMemoryStream(_entry);
		}

		public Stream OpenWrite(bool truncate)
		{
			if (IsReadOnly)
				throw new NotSupportedException("Stream provider is read only.");

			var stream = OpenMemoryStream(_entry);
			if (truncate)
				stream.SetLength(0);

			return stream;
		}

		/// <summary>
		/// Opens an entry stream and copies it to a new memory stream. This prevents errors
		/// when opening multiple files in a zip archive.
		/// </summary>
		public Stream OpenMemoryStream(ZipArchiveEntry entry)
		{
			using (var inStream = entry.Open())
			{
				var outStream = new MemoryStream();
				inStream.CopyTo(outStream);
				outStream.Seek(0, SeekOrigin.Begin);
				return outStream;
			}
		}
	}
}
